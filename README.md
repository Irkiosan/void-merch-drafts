# void-merch-drafts

Here you find artwork to make your own Void Linux merchandise. Each design is provided as a vector file as well as a corresponding png file. All files are created by myself. The Void logo (and Void text) are recreated as closely as possible according to the original Void Linux artwork. All credit for the Void logo goes to the creater of the logo.

Find yourself a trusted print service and have fun with your Void Linux merchandise.

### Content

- svg file (draft)
- corresponding png file (200 dpi)

### Features

- transparency
- versions for dark and bright backgrounds